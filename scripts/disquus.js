var body = $('body:first');

// Observe mutations of posts in Disqus iframe
new MutationObserver(function (mutations) {
    mutations.forEach(function (mutation) {
        var node;
        $.each(mutation.addedNodes, function () {
            node = $(this).find("#post-list");
            if(node.length) {
              observePostList(node);
            }
        })
    })
}).observe(body[0], {childList: true});

var observePostList = function(postList) {
  new MutationObserver(function (mutations) {
      mutations.forEach(function (mutation) {
        var node;
        for (var i = 0; i < mutation.addedNodes.length; i++) {
          node = $(mutation.addedNodes[i]);
          if (node.hasClass("post")) {
            processPost(node);
          } else if (node.hasClass('post-content')) {
            processPost(node.parents('.post'));
          }
        }
      })
  }).observe(postList[0], {childList: true, subtree: true, attributes: true});
}

var processPost = function(post) {
  var postContent = post.find('.post-content:first');
  var upvote = post.find('.vote-up:first');
  var downvote = post.find('.vote-down:first');
  var upvoteIcon = upvote.find('i.icon-arrow-2:first');
  var downvoteIcon = downvote.find('i.icon-arrow:first');

  $.each(post.children('ul.children').children('li.post'), function() {
    processPost($(this));
  });

  if (post.hasClass("minimized") || postContent.hasClass('processed-dixqus')) {
    return;
  }

  postContent.addClass('processed-dixqus');

  loadAfter('templates/brohoof.html', upvoteIcon, function(brohoofUp) {
    loadAfter('templates/brohoof.html', downvoteIcon, function(brohoofDown) {
      var updateBrohooves = function() {
        setTimeout(function() {
          if (upvote.hasClass('upvoted')) {
            brohoofUp.attr('src', localURL('resources/brohoofed-left.png'));
          } else {
            brohoofUp.attr('src', localURL('resources/brohoof-left.png'));
          }

          if (downvote.hasClass('downvoted')) {
            brohoofDown.attr('src', localURL('resources/brohoofed-right.png'));
          } else {
            brohoofDown.attr('src', localURL('resources/brohoof-right.png'));
          }
        }, 50)};

      downvote.click(updateBrohooves);
      upvote.click(updateBrohooves);
      updateBrohooves();

      upvoteIcon.hide();
      downvoteIcon.hide();
    });
  });
}

/*
  Gets post element from a child of the post
*/
var postOf = function(postChild) {
  return postChild.parents('.post:first');
}
