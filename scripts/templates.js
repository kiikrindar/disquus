/*
  Loads in an element from a URL to an HTML template and inserts it after
  and element
*/
var loadAfter = function(htmlURL, element, callbackFunction = function(htmlElement){}) {
  var htmlElement;

  $.get(localURL(htmlURL), function(htmlStr) {
    htmlElement = $(htmlStr);
    htmlElement.insertAfter(element);

    callbackFunction(htmlElement);
  });
}

/*
  Loads in an element from a URL to an HTML template and inserts it before
  and element
*/
var loadBefore = function(htmlURL, element, callbackFunction = function(htmlElement){}) {
  var htmlElement;

  $.get(localURL(htmlURL), function(htmlStr) {
    htmlElement = $(htmlStr);
    htmlElement.insertBefore(element);

    callbackFunction(htmlElement);
  });
}

/*
  Uses chrome API to correctly load things from local space
*/
var localURL = function(URL) {
  return chrome.extension.getURL(URL);
}
